from django.db import models

# Create your models here.
class Encuesta(models.Model):
	pregunta = models.CharField(max_length=255)
	fecha_creacion = models.DateTimeField('date published', auto_now_add=True)

	def __unicode__(self):
		return self.pregunta

	def was_publised_recently(self):
		pass
		"TODO"

	def get_all_choices(self):
		return self.choice_set.all()

class Choice(models.Model):
	encuesta = models.ForeignKey(Encuesta)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)

	def __unicode__(self):
		return u"%s | %s" % (self.encuesta.pregunta, self.choice_text)
