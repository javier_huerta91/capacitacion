from django.contrib import admin

# Register your models here.
from polls.models import Encuesta, Choice

class EncuestaAdmin(admin.ModelAdmin):
	list_display = ('pregunta', 'fecha_creacion')
	list_filter = ['pregunta']
	search_fields = ['pregunta']

admin.site.register(Encuesta, EncuestaAdmin)
admin.site.register(Choice)

