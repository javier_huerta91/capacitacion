from django.shortcuts import render
from polls.models import Encuesta


# Create your 
def home(request):
	context = {}
	context['encuestas'] = Encuesta.objects.all()
	return render(request, 'polls/home.html', context)

def contacto(request):
	context = {}
	#context['contacto'] = Encuesta.objects.all()
	return render(request, 'polls/contacto.html', context)

