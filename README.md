# Comenzando un Proyecto Django

### Algunas configuraciones previas
* Instalar Python 2.7
* Instalar easy_install (setup-tools)
* Agregar gestor de paquetes python pip
* Instalar virtualenv con pip ---> pip install virtualenv
* Crear virtualenv --> virtualenv "nombre_entorno_virtual_python"
* Activar entorno Virtual
> utilizando el comando source de linux ---> source nombre_entorno_virtual_python/bin/activate
* Instalar archivo de requerimientos python
> pip install -r requirements.txt

### Correr el servidor de prueba
> python manage.py runserver